const folder = "tailwind_repo";

const config = {
  base: "src/",

  sass: {
    src: "src/stylesheet/**/*.scss",
    dest: "src/css",
    options: {
      includePaths: ["bower_components", "node_modules"]
    }
  },

  autoprefixer: {
    browsers: ["ie 10", "last 2 versions"]
  },

  postcss: {
    src: "src/css/main.css"
  },

  js: {
    src: "src/js/**/*.js"
  },

  php: {
    src: "src/**/*.php"
  },

  browserSync: {
    options: {
      proxy: "http://localhost/" + folder + "/src",
      ui: {
        port: 2134
      }
    }
  },

  images: {
    src: "src/images/**/*.+(png|jpg|jpeg|gif|svg)",
    dest: "dist/images/",
    options: {
      name: "project",
      optimizationLevel: 7,
      progressive: true,
      interlaced: true,
      multipass: true,
      svgoPlugins: [{ removeViewBox: true }],
      verbose: true
    }
  },

  useref: {
    src: ["src/**/*.php", "src/index.php"],
    dest: "dist",
    options: {
      searchPath: "src/"
    }
  },

  uglify: {
    options: {
      mangle: false
    }
  },

  cleancss: {
    options: {
      debug: true
    }
  },

  uncss: {
    src: "src/css/main.css",
    options: {
      html: ["http://localhost/" + folder + "/src/index.php"],
      ignore: [
        /.js/,
        /.aos/,
        /.feature/,
        /.l-popup/,
        /.c-popup/,
        /.is-/,
        /.u-/,
        /.c-/,
        /.o-/,
        /.cookies/,
        ".header--solid"
      ],
      report: true
    }
  },

  extras: {
    src: [
      "src/*.*",
      "!src/*.php",
      "!src/favicon.png",
      "!src/Thumbs.db",
      "node_modules/apache-server-configs/dist/.htaccess"
    ]
  },

  favicons: {
    src: "src/favicon.png",
    dest: "./dist/favicons",
    options: {
      appName: "Unestudio - estudio arquitectura",
      appDescripcion: "",
      background: "#020307",
      path: "favicons/",
      icons: {
        android: true,
        appleIcon: true,
        appleStartup: false,
        coast: false,
        favicons: true,
        firefox: true,
        opengraph: true,
        twitter: true,
        windows: true,
        yandex: false
      },
      url: "http://www.unestudio.com/",
      display: "standalone",
      orientation: "portrait",
      logging: true,
      html: "./dist/index.php",
      replace: true
    }
  }
};

module.exports = config;
