const gulp = require('gulp')

const config = require('../config')

gulp.task('extras', _ => {
  return gulp.src(config.extras.src)
    .pipe(gulp.dest('dist'))
})
