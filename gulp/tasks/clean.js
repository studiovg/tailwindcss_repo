const gulp = require('gulp')
const del = require('del')

gulp.task('clean:dev', () => del('src/css'))
gulp.task('clean:dist', () => del.sync('dist'))
