const gulp = require('gulp')
const $ = require('gulp-load-plugins')()

const config = require('../config')

gulp.task('images', _ => {
  return gulp.src(config.images.src)
    .pipe($.cache($.imagemin(), config.images.options))
    .pipe(gulp.dest('dist/images'))
})

gulp.task('cache:clear', callback => {
  return $.cache.clearAll(callback)
})
