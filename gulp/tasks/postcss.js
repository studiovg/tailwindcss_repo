const gulp = require("gulp");
const precss = require("precss");
const cssnext = require("postcss-cssnext");
const uncss = require("postcss-uncss");
const $ = require("gulp-load-plugins")();
const tailwindcss = require("tailwindcss");

const config = require("../config");

gulp.task("postcss:dev", ["sass"], () => {
  return gulp
    .src(config.postcss.src)
    .pipe($.sourcemaps.init())
    .pipe($.postcss([cssnext(), precss(), tailwindcss("./tailwind.js")]))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(config.base + "/css"));
});

gulp.task("postcss:dist", ["sass"], () => {
  return gulp
    .src(config.postcss.src)
    .pipe(
      $.postcss([
        cssnext(config.autoprefixer.options),
        precss(),
        tailwindcss("./tailwind.js"),
      ])
    )
    .pipe(gulp.dest(config.base + "/css"));
});