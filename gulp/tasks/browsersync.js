const gulp = require('gulp')
const browserSync = require('browser-sync')

const config = require('../config')

gulp.task('browserSync', _ => browserSync(config.browserSync.options))
gulp.task('reload', _ => browserSync.reload)
