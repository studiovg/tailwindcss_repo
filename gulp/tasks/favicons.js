const gulp = require('gulp')
const $ = require('gulp-load-plugins')()

const config = require('../config')

gulp.task('favicons', _ => {
  gulp.src(config.favicons.src)
    .pipe($.favicons(config.favicons.options))
    .pipe(gulp.dest(config.favicons.dest))
})
