const gulp = require('gulp')
const browserSync = require('browser-sync')
const runSequence = require('run-sequence')

const config = require('../config')

gulp.task('watch', _ => {
  gulp.watch(config.sass.src, function () {
    runSequence(
      ['clearScreen', 'postcss:dev'],
      'reload'
    )
  })
  gulp.watch(config.js.src, ['watch-js'])
  gulp.watch(config.php.src, browserSync.reload)
})

gulp.task('watch-js', browserSync.reload)
