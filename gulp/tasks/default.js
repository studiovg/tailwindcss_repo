const gulp = require('gulp')
const runSequence = require('run-sequence')

gulp.task('default', callback => {
  runSequence('clean:dev', 'postcss:dev', ['browserSync', 'watch'], callback)
})
