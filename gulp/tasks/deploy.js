const fs = require('fs')
const gulp = require('gulp')
const rsync = require('rsyncwrapper')

const creds = JSON.parse(fs.readFileSync('./gulp/secrets.json'))

gulp.task('rsync', () => {
  const uploadFolder = creds.dest || 'carpetaTemporal'

  rsync({
    src: 'dist/',
    dest: `${creds.username}@${creds.server}:${uploadFolder}`,
    recursive: true,
    deleteAll: true,
    ssh: true
  }, function (error, stdout, stderr, cmd) {
    if (error) {
      console.log(error.message)
    } else {
      console.log('Sincronizacion correcta.')
    }
  })
})
