const $ = require('gulp-load-plugins')()
const gulp = require('gulp')
const browserSync = require('browser-sync')
const customPlumber = require('../custom-modules/plumber')

const config = require('../config')

gulp.task('sass', function () {
  return gulp.src(config.sass.src)
    .pipe(customPlumber('Error Running Sass'))
    .pipe($.sass({ includePaths: config.sass.options.includePaths }))
    .pipe(gulp.dest(config.sass.dest))
    .pipe(browserSync.reload({ stream: true }))
})
