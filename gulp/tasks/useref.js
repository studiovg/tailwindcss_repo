const gulp = require("gulp");
const $ = require("gulp-load-plugins")();

const config = require("../config");

gulp.task("useref", _ => {
  return gulp
    .src(config.useref.src)
    .pipe($.useref(config.useref.options))
    .pipe($.if("*.js", $.sizediff.start()))
    .pipe($.if("*.js", $.babel()))
    .pipe($.if("*.js", $.uglify(config.uglify.options)))
    .pipe($.if("*.js", $.sizediff.stop({ title: "js" })))
    .pipe(gulp.dest("dist"));
});
