const $ = require('gulp-load-plugins')()

function customPlumber (errTitle) {
  return $.plumber({
    errorHandler: $.notify.onError({
      title: errTitle || 'Error running Gulp',
      message: 'Error: <%= error.message %>',
      sound: true
    })
  })
}

module.exports = customPlumber
