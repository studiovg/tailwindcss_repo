<div class="swiper-container">
  <!-- Additional required wrapper -->
  <div class="swiper-wrapper h-screen">
    <!-- Slides -->
    <div class="swiper-slide h-screen">
      <img src="images/presentacion-unestudio1.jpg" alt="">
    </div>
    <div class="swiper-slide h-screen">
      <img src="images/presentacion-unestudio2.jpg" alt="">
    </div>
    <div class="swiper-slide h-screen">
      <img src="images/presentacion-unestudio3.jpg" alt="">
    </div>
  </div>

  <!-- If we need navigation buttons -->
  <div class="swiper-button-prev"></div>
  <div class="swiper-button-next"></div>

</div>
<!--
<section class="h-screen relative">
  <div class="h-full bg-cover" style="background-image: url('images/presentacion-unestudio1.jpg')"></div>
</section> -->

<div class="relative p-4 md:p-10 lg:p-32">
  <div class="container max-w-2xl mx-auto">

    <div class="flex flex-wrap -mx-6">
      <?php for($i=1; $i<10; $i++) { ?>

      <div class="w-full sm:w-1/2 md:w-1/3 px-6 mb-12">
        <a class="c-work" href="index.php?sec=work&amp;id=<?php echo $i ?>">
          <div class="c-work__img overflow-hidden">
            <div style="background-image:url('trabajos/<?php echo $i?>.jpg')"></div>
          </div>
          <h2 class="c-work__title">
            <span class="font-bold">//</span> lorem ipsum dolor
          </h2>
        </a>
      </div>

      <?php } ?>
    </div>

  </div>
</div>


<!-- <modal :class="{'is-active': contactoEnviado}" @ocultar="contactoEnviado = false" titulo="Formulario enviado!">
  <p>
    Su mensaje ha sido enviado con éxito. En breve nos pondremos en contacto con usted.
  </p>
</modal>

<modal :class="{'is-active': mostrarAvisoLegal}" @ocultar="mostrarAvisoLegal = false" titulo="Aviso legal">
  <p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat, laborum. Saepe atque, omnis hic inventore vitae eos, dolorum,
    dignissimos labore enim repudiandae quod voluptates incidunt adipisci natus sint! Excepturi, voluptatem.
  </p>
</modal>


<modal :class="{'is-active': isWorkOpen}" @ocultar="isWorkOpen = false" titulo="Work with Us!">
  <p>
    En Speak and Think, contamos con los mejores profesionales.
  </p>
  <p>
    Si quieres formar parte de nuestro equipo, envíanos tu curriculum y nos pondremos en contacto contigo para entrevistarte
    personalmente.
  </p>
</modal> -->