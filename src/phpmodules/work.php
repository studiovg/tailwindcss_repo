<section class="c-work-sheet">
    <div class="c-work-sheet__content">
        <div class="c-work-sheet__info bg-grey-lightest">
            <div class="flex">

                <div class="p-6">
                    <h2 class="text-black">Abella</h2>
                    <span class="text-grey-darker">Santiago de compostela</span>
                    <div class="mt-4 pt-4 border-t border-grey text-sm text-grey-darker leading-normal">
                        <p class="mb-4">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur, nihil dignissimos amet voluptas maxime beatae deleniti?
                            Facilis repellat suscipit soluta nemo optio beatae inventore. Facere rem ad dicta saepe numquam.
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur, nihil dignissimos amet voluptas maxime beatae deleniti?
                            Facilis repellat suscipit soluta nemo optio beatae inventore. Facere rem ad dicta saepe numquam.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="c-work-sheet__media">
            <img class="w-full mb-5" src="trabajos/1.jpg" alt="">
            <img class="w-full mb-5" src="trabajos/1.jpg" alt="">
            <img class="w-full mb-5" src="trabajos/1.jpg" alt="">
        </div>
    </div>
</section>