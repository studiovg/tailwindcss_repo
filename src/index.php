<!doctype html>
<html class="no-js" lang="es">

<head>

  <!-- build:css css/main.css -->
  <link rel="stylesheet" href="css/main.css">
  <!-- endbuild -->

  <link href="https://fonts.googleapis.com/css?family=Catamaran|Poppins:400,800" rel="stylesheet">
</head>

<body>

  <!--[if lt IE 10]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->

  <div id="jsVueApp">

    <header class="container h-18 bg-white px-8 py-4 fixed w-full z-50 jsMainHeader">

      <div class="flex items-center">

        <div class="flex-1">

          <nav class="hidden">

            <ul class="list-reset flex items-center">
              <li class="mr-6">
                <a href="#presentacion" class="c-link  jsGoto">Proyecto</a>
              </li>
              <li class="mr-6">
                <a href="#que-ofrecemos" class="text-grey-dark hover:text-black no-underline uppercase tracking-wide text-xs  jsGoto">Nosotros</a>
              </li>
              <li class="mr-6">
                <a href="#certificaciones" class="text-grey-dark hover:text-black no-underline uppercase tracking-wide text-xs  jsGoto">Contacto</a>
              </li>
            </ul>

          </nav>

          <a href="#" class="h-6 w-10 flex flex-col justify-between">
            <span class="block bg-grey-darkest h-1 w-full"></span>
            <span class="block bg-grey-darkest h-1 w-full"></span>
            <span class="block bg-grey-darkest h-1 w-full"></span>
          </a>


        </div>

        <div class="">
          <h1>
            <a class="block" href="index.php" title="Inicio">
              <img src="images/unestudio-logo.svg">
            </a>
          </h1>
        </div>

        <div class="flex-1 text-right">
        </div>

      </div>



    </header>


    <div class="c-curtain" v-bind:class="{'is-open': offsetPanelActive}" @click="offsetPanelActive = false"></div>


    <main class="c-main px-8">
      <?php
        $sec = (empty($_GET['sec'])) ? 'home' : $_GET['sec'] ;
        $path = "phpmodules/".$sec.".php";
        require $path;
      ?>
    </main>



    <footer class="border-t border-grey-lightest fixed pin-b w-full z-50 text-xs flex justify-between uppercase p-4 bg-white text-grey-darkest">

      <ul class="list-reset flex">
        <li class="mr-10">
          <div class="flex">
            <div class="flex-0 mr-1">
              <i class="fa fa-phone"></i>
            </div>
            <div class="flex-1">
              + 34 981 69 74 38
            </div>
          </div>
        </li>
        <li class="mr-10">
          <div class="flex">
            <div class="flex-0 mr-1">
              <i class="fa fa-home"></i>
            </div>
            <div class="flex-1">
              Carreira 33, Bajo - Zas (La Coruña)
            </div>
          </div>
        </li>
      </ul>

      <ul class="list-reset flex">
        <li class="mr-2">
          <i class="fa fa-facebook"></i>
        </li>
        <li class="mr-2">
          <i class="fa fa-instagram"></i>
        </li>
        <li class="mr-2">
          <i class="fa fa-linkedin"></i>
        </li>
        <li class="mr-2">
          <i class="fa fa-twitter"></i>
        </li>
      </ul>

  </div>

  </footer>

  <!-- <vgcookies></vgcookies> -->

  </div>



  <!-- build:js js/main.js -->
  <script src="js/main.js"></script>
  <!-- endbuild -->


</body>

</html>